use std::env;
use std::fs::File;
use std::io::Write;
use std::str::FromStr;
use std::collections::HashMap;
use std::io::{BufRead,BufReader};

pub fn functions(){
    get_file();
}

struct Person{
    first_name:String,
    paid_amount:i32
}

fn get_file(){
    let args : Vec<String> = env::args().collect();
    let input = &args[1];
    let output = &args[2];
    open_read_file(input,output);
}

fn open_read_file(i:&str, o:&str){
    let file = File::open(i).expect("Path does not already exist !");
    let reader =BufReader::new(file);
    let mut new_file = File::create(o).expect("Output file could not be created");
    let mut list = HashMap::new();
    for line in reader.lines(){
        let line = line.expect("Line not found !");
        let words : Vec<&str> = line.split(",").collect();
        let mut fname = words[0].to_string();
        let amount :i32 = FromStr::from_str(words[2]).expect("Index out of bound !");
        fill_list(&mut fname,amount,&mut list);
    }
    write_file(&mut list,&mut new_file);
}

fn fill_list(fname:&mut String, amount:i32, list: &mut HashMap<String, i32>){
    if list.contains_key(fname){
        let namount = amount + list.get(fname).expect("There is no reference to the value corresponding to the key !");
        list.insert(fname.to_string(),namount);
    }
    else {
         list.insert(fname.to_string(),amount);
    }   
}

fn write_file(list: &mut HashMap<String, i32>, file : &mut std::fs::File){
    for (key,val) in list.iter(){
        let p = Person {
            first_name : key.to_string(),
            paid_amount : *val,
        };
        file.write_all(p.first_name.as_bytes()).expect("`p.first_name` couldn't write into file");
        file.write_all(",".as_bytes()).expect("`,` couldn't write into file");
        file.write_all((p.paid_amount).to_string().as_bytes()).expect("`p.paid_amount` couldn't write into file");
        file.write(b"\n");
    }     
}